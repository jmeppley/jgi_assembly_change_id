FROM continuumio/miniconda3:latest

MAINTAINER John Eppley <jmeppley@gmail.com>

# reduce image size by removing test data after clone
RUN git clone https://jmeppley@bitbucket.org/jmeppley/jgi_assembly_change_id.git \
	      /opt/jgi_assembly_change_id

# reduce image size after conda run
RUN conda env update -n root -f /opt/jgi_assembly_change_id/conda.yml && \
  conda clean --all -y

WORKDIR /workdir

ENTRYPOINT [ "/usr/bin/tini", "--", "snakemake", "-s", "/opt/jgi_assembly_change_id/Snakefile", "-p" ]

