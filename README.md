# JGI metagenomic assemblies #

This package has scripts for manipulating metagenomic assemblies in the IMG-next format

## Renaming ##

The root folder contains a python makefile (Snakefile) for renaming all identifiers in an assembly. This includes:

 * The root folder name
 * File names
 * Sequence names

### Installation ###

Once this repo is cloned, install the necessary software with the conda.yml file. Then you can run a few ways:

 * Snakefile: instructions in the comments
 * modify_one_assembly.sh $NEW_ID $OLD_ID
 * modify_many_assemblies.sh $ID_MAP
 (In this last case, ID_MAP is a tab separated text file with two columns: new IDs and old IDs)
 
## Swift ##

The swift subdirectory contains a script to upload assembly folders to Swift.

