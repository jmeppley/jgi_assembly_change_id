#!/bin/bash

## SGE/UGE configuration
#$ -cwd
# -- 58 samples took 10 hours
#$ -l h_rt=10:00:00
# -- Use an entire node
#$ -l exclusive.c

## COMMAND LINE ARGUMENTS
# qsub modify_one_assembly.sh $ID_MAP [...]
ID_MAP=$1                   # tab sep table with new ids in col 1 and old ids in 2

## OPTIONS (positional)
# where to find old assemblies
SOURCE_DIR=${2:-/projectb/scratch/torben/Metadone}
# where to find this script (and makefile)
REPO_DIR=${3:-/projectb/scratch/jmeppley/modified_assemblies/jgi_assembly_change_id}
# where to find conda env (there should be one in REPO_DIR/conda.env)
CONDA_ENV=${4:-${REPO_DIR}/conda.env}    # Default to env in repository

# make sure we have the correct python/anaconda
module unload python || true
module load python/3.5-anaconda_4.2.0

# activate conda environment
echo "Activating CONDA env"
CONDA_ENV=$(readlink -f ${CONDA_ENV})    # Make sure path is absolute
source activate $CONDA_ENV || true
if [ "$CONDA_PREFIX" != "$CONDA_ENV" ]; then
	echo "ERROR activating conda env $CONDA_ENV"
	exit 1
fi

# run snakemake
date
CMD="snakemake -s $REPO_DIR/Snakefile -p -j 20 --config id_mapping_table=$ID_MAP  source_dir=$SOURCE_DIR"
echo running: $CMD
eval time $CMD
date

