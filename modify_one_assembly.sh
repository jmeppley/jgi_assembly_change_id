#!/bin/bash

## SGE/UGE configuration
##$ -cwd
##$ -l h_rt=6:00:00
## -- The makefile uses 2.5 threads with -j 3
##$ -pe pe_slots 2

## SLURM config for cori
#SBATCH -t 6:00:00
#SBATCH -C haswell
#SBATCH -p shared
#SBATCH -n 1
#SBATCH -c 2
#SBATCH -L projectb
export OMP_NUM_THREADS=2

## COMMAND LINE ARGUMENTS
# qsub modify_one_assembly.sh $NEW_ID $OLD_ID [...]
NEW_ID=$1
OLD_ID=$2

## OPTIONS (positional)
# where to find old assemblies
SOURCE_DIR=${3:-/projectb/scratch/torben/Metadone}
# where to find this script (and makefile)
REPO_DIR=${4:-/projectb/scratch/jmeppley/modified_assemblies/jgi_assembly_change_id}
# where to find conda env (there should be one in REPO_DIR/conda.env)
CONDA_ENV=${5:-${REPO_DIR}/conda.env}    # Default to env in repository

# make sure we have the correct python/anaconda
if [ "$NERSC_HOST" == "cori" ]
then
  module unload python || true
  module load python/3.6-anaconda-4.4
fi
if [ "$NERSC_HOST" == "genepool" ]
then
  module unload python || true
  module load python/3.6-anaconda_4.3.0
fi

# activate conda environment
echo "Activating CONDA env"
CONDA_ENV=$(readlink -f ${CONDA_ENV})    # Make sure path is absolute
source activate $CONDA_ENV || true
if [ "$CONDA_PREFIX" != "$CONDA_ENV" ]; then
	echo "ERROR activating conda env $CONDA_ENV"
	exit 1
fi

# run snakemake
date
CMD="snakemake -s $REPO_DIR/Snakefile -p -j 3 --config new_id=$NEW_ID old_id=$OLD_ID source_dir=$SOURCE_DIR --nolock"
echo running: $CMD
eval time $CMD
date

