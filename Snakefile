# Rename an assembly:
#  * create a copy of an existing assembly
#  * modify all named features to change old asseembly id to new id
#  * optional report of places that the old id still exists (Should just be comments and logs)
#
# requirements:
#  * python 3 (needed by snakemake)
#  * snakemake
#  * pandas (would be easy to bypass)
#
# Running:
#
#  This is expected to be run from the output directory. It will create one folder in the current
#  working directory for each renamed assembly. The folder will be named with the new ID and the
#  contentents will be the modified files.
#
#   $ snakemake [-s path/to/Snakefile] [OPTIONS] [CONFIGURATION]
#
# The path to the makefile can be omitted if the file is ./Snakefile
#
# OPTIONS:
#  snakemake options to modify the workflow
#   -p             print any shell commands used
#   -j THREADS     number of threads to allocate
#   -n             dry run only, don't create any files
#   --verbose      print LOTS of messages
#
# CONFIGURATION:
#  specify the id translation data and the location of the original assemblies
#
#  for one assembly, EG:
#
#   $ snakemake -s path/to/Snakefile -p -j 2 --config new_id=X12345 old_id=87654 source_dir=/projectb/scratch/torben/Metadone
#
#  (If BAM files are involved, one thread can process all the other files in less time than the BAM file)
#
#  for multiple assemblies, use a table of IDs, EG:
#
#   $ snakemake -s path/to/Snakefile -p -j 20 --config id_mapping_table=path/to/id_mapping.tab source_dir=/projectb/scratch/torben/Metadone
#
#  where path/to/id_mapping.tab is a tab separated 2-column table of new and old IDs. EG:
#X12345   87645
#X12346   503918
#...
#
#  or put everything in a yaml or json file:
#
#   $ snakemake -s path/to/Snakefile -p -j 20 --configfile=path/to/config.yaml
#
# where the config file is something like:
#source_dir: /projectb/scratch/torben/Metadone
#id_map:
#  X12345: 87645
#  X12346: 503918
#  ...

import os, glob, gzip, yaml, pandas

## CONFIGURATION
# modify values below with a configfile or using --config key=value in command

# old/new id pairs can be specified 3 ways:
#  1) set a single pair with: --config old_id=123 new_id=XYZ
#  2) provide a 2 column table (no headers, col_1=new, col_2=old)
#          with: --config id_mapping_table=FILE
#  3) provide a config file (--configfile=FILE) containing 'id_map' that maps from new to old ids

# new and old IDs must be configured
if 'id_map' not in config:
    if 'id_mapping_table' in config:
        config['id_map'] = dict(pandas.read_table(config['id_mapping_table'],
                                                  names=['new','old'],
                                                  index_col=0,
                                                  header=None)['old'])
    else:
        try:
            old_id = str(config['old_id'])
            new_id = str(config['new_id'])
            config['id_map'] = {new_id: old_id}
        except KeyError:
            raise Exception("Please supply a table of old and new ids with \n"
                            "    --config id_mapping_table=FILE \n"
                            " or a single pair of IDs with \n"
                            "    --config old_id=123 new_id=XYZ")

# You should also set the source and destination locations
source_dir = config.setdefault('source_dir', '/projectb/scratch/torben/Metadone')

# set run_report to False to skip accounting of places where the old ID still exists
run_report = bool(config.setdefault('run_report', True))

## SETUP
reverse_id_map = {o:n for n,o in config['id_map'].items()}
target_files = config.setdefault('_TARGETS', [])
file_checks = config.setdefault('_FILE_CHECKS', {})
report_dir = config.setdefault('_REPORT_DIRS', {})
missing_assembly_dirs = []


## excluding files from translation
# functions for checking files
excludes_functions = [
    os.path.islink,                 # skip links
]
# list of regular expressions to exclude files from translation
excludes_expressions = [re.compile(pattern) for pattern in [
    r'_contigs\.fasta\.[a-z]+$',    # skip index of contigs
    r'\.bai$',                      # skip index of BAM file
    r'/core-[^/]+$',                # skip core files
]]
# combine in single function
def file_not_excluded(file_name):
    """ Should we copy/rename file? """
    for excl_func in excludes_functions:
        if excl_func(file_name):
            return False
    for excl_expr in excludes_expressions:
        if excl_expr.search(file_name):
            return False
    return True

# for each assembly
for new_id in config['id_map']:
    old_id = str(config['id_map'][new_id])
    new_id = str(new_id)
    report_dir[new_id] = '.renaming_{}_to_{}'.format(old_id, new_id)

    # check that the assembly exists
    if not os.path.isdir(os.path.join(source_dir, old_id)):
        missing_assembly_dirs.append(old_id)
        continue

    # files to create for this ID (skips index files and links)
    assembly_files = [os.path.join(new_id,
                                   re.sub(old_id, new_id, os.path.basename(f))) \
                      for f in glob.glob(os.path.join(source_dir, old_id, "*")) \
                      if file_not_excluded(f) ]

    if len(assembly_files)==0:
        logger.warning("NO ASSEMBLY FILES FOUND!\nMake sure the assembly exists and\n "
                       "that you specify the new_id first and old_id second in your\n "
                       "table or config file. \nYou may also want to check your "
                       "'source_dir' configuration."
                      )

    # generate names of count files to create
    target_files.extend(assembly_files)
    file_checks[new_id] = [os.path.join(path, report_dir[new_id], fname + ".count") \
                           for (path, fname) in map(os.path.split, assembly_files)]

    if run_report:
        # Add report to taargets if requested
        target_files.append('{new_id}/{report_dir}/report.txt'.format(
            new_id=new_id,
            report_dir=report_dir[new_id]
        ))

if len(missing_assembly_dirs) > 0:
    raise Exception("{} ASSEMBLY FOLDER[S] MISSIN: {}!\nMake sure the assembly or assemblies\n "
                    "exist[s] and that you specify the new_id first and old_id second in your\n "
                    "table or config file. \nYou may also want to check your "
                    "'source_dir' configuration.".format(len(missing_assembly_dirs),
                                                         ", ".join(missing_assembly_dirs),
                                                        ))

logger.debug(yaml.dump(config))

## Default rule
rule output:
    "Simply list the files we want to create"
    input: target_files

## Restrict some matches:
wildcard_constraints:
    new_id=r'[^\/]+',
    report_dir=r'[^\/]+',
    file_nam=r'[^\/]+'

## special funtctions for handling different files
def get_cat_program(wildcards):
    """
    this is a special function that can be use in the params field to
    define a command based on the wildcard values

    Here we specify programs to unpack binary formats
    """
    if wildcards.file_name.endswith('.gz'):
        # use gunzip for .gz files
        return 'gunzip -c'
    elif wildcards.file_name.endswith('.bam'):
        # use samtools to get text (SAM) version for processing
        return 'samtools view -h'
    else:
        # just use cat for basic files
        return 'cat'

def get_uncat_program(wildcards):
    """
    this is a special function that can be use in the params field to
    define a command based on the wildcard values

    Here we return programs to re-pack binary formats
    """
    if wildcards.file_name.endswith('.gz'):
        # use gzip to repack .gz files
        return 'gzip -c'
    elif wildcards.file_name.endswith('.bam'):
        # use samtools to turn SAM back into ninary BAM
        return 'samtools view -b - '
    else:
        # just use cat for basic files
        return 'cat'

def get_rename_regexp(wildcards):
    """ Return a regular expression that will replace old ids with new """
    return r's/\b{old_id}_contig/{new_id}_contig/g'\
                    .format(old_id=config['id_map'][wildcards.new_id],
                            new_id=wildcards.new_id)

rule rename_elements:
    """
    Files whose names start with the old id, should have their contents modified.
    """
    input: lambda w: '{0}/{1}/{1}_{2}'.format(source_dir, config['id_map'][w.new_id], w.file_name)
    output: '{new_id}/{new_id}_{file_name}'
    benchmark: '{new_id}/.benchmarks/{new_id}_{file_name}.time'
    params:
        cat=get_cat_program,
        uncat=get_uncat_program,
        regexp=get_rename_regexp,
    threads: lambda w: 2 if w.file_name.endswith('.bam') or w.file_name.endswith('.gz') \
                         else 1
    shell:
        '{params.cat} {input} \
         | perl -pe "{params.regexp}" \
         | {params.uncat} \
         > {output}'

rule copy_file:
    """
    Copy other files
    """
    input: lambda w: '{}/{}/{}'.format(source_dir, \
                                       config['id_map'][w.new_id], \
                                       re.sub(w.new_id, \
                                              config['id_map'][w.new_id], \
                                              w.file_name))
    output: '{new_id}/{file_name}'
    benchmark: '{new_id}/.benchmarks/{file_name}.time'
    shell: 'cp {input} {output}'

# files that match both rules, should be renamed:
ruleorder: rename_elements > copy_file

## REPORTING
rule report:
    """ for renamed files that still have old_id, print example lines """
    input: lambda w: file_checks[w.new_id]
    output: '{new_id}/{report_dir}/report.txt'
    benchmark: '{new_id}/.benchmarks/{report_dir}.report.time'
    run:
        if not isinstance(output, str):
            output_file = output[0]
        else:
            output_file = output
        with open(output_file, 'w') as REPORT:
            for file_check in input:
                with open(file_check) as CHECK:
                    count = int("".join(CHECK.readlines()).strip())
                if count>0:
                    # remove report_dir name and ".count" from end (last 6 chars)
                    original_file = re.sub(report_dir[wildcards.new_id] + os.path.sep, '', file_check)[:-6]
                    REPORT.write("{} has {} instances of the old id {}. EG:\n".format(
                        original_file,
                        count,
                        config['id_map'][wildcards.new_id],
                    ))
 
                    snippet_file = file_check + ".snippet"
                    with open(snippet_file) as F:
                        for line in F:
                            REPORT.write(line)
                    REPORT.write("____\n\n")

rule count_old_ids:
    """
    use grep to count how many times the old id shows up in the modified file

    The complicated shell at the end does two things:
     1) if there is a match, save first 3 matched lines to .snippet file
     2) if there is no match, change exit code from 1 to 0 so snakemake doesn't freak out
    """
    input: '{new_id}/{file_name}'
    output: '{new_id}/{report_dir}/{file_name}.count'
    benchmark: '{new_id}/.benchmarks/{report_dir}.{file_name}.count.time'
    params:
        cat=get_cat_program,
        old_id=lambda w: config['id_map'][w.new_id]
    shell:
        """
        COUNT=`{params.cat} {input} | grep -c {params.old_id}` \
          || ( e=$?; if [ "$e" -eq "1" ]; then exit 0; else exit $e; fi)
        echo $COUNT > {output}

        if [ "$COUNT" -ne "0" ]; then
          {params.cat} {input} | grep -m 3 {params.old_id} > {output}.snippet || exit 0;
        fi
        """
