#!/bin/bash
REPO_DIR=`dirname $0`
CONDA_ENV=${1:-${REPO_DIR}/conda.env}    # Default to env in repository
CONDA_ENV=$(readlink -f ${CONDA_ENV})    # Make sure path is absolute
CONDA_YML=${REPO_DIR}/conda.yml

# make sure we have the correct python/anaconda
module unload python || true
module load python/3.5-anaconda_4.2.0

# Create conda environment if needed
CONDA_ENV=$(readlink -f ${REPO_DIR}/conda.env)
CONDA_YML=${REPO_DIR}/conda.yml
if [ "$CONDA_YML" -nt "$CONDA_ENV" ]; then
	echo "Creating CONDA env once in $CONDA_ENV"
	rm -rf "$CONDA_ENV"
	conda env create -f $CONDA_YML -p $CONDA_ENV --force --quiet
else
    echo "Envoronment in $CONDA_ENV is newer than specification in $CONDA_YML. "
    echo "Leaving it alone."
fi

