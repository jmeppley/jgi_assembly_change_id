#!/usr/bin/env python
"""
Upload Assembl(y|ies) to Swift Openstack

An assembly is specified as a path where the assembly id is the last
element of the path. EG: assembly 10001 is in /projects/Metadone/10001

All the files in an assembly folder will be uploaded to a container
with the name "assembly_{id}" where {id} is the assembly id. The
naming prefix can be changed in the options.

As per the swift CLI, server details can be set in your env. EG:

export ST_AUTH=http://production01.acme.com/auth/v1.0
export ST_USER=user01
export ST_KEY=password

They can also be passed as arguments (see below). If only the password is
missing, the script will ask for user input.

Usage:
    upload_assembly_to_swift.py [options] <assembly>...

Options:
    -h --help                  Show this help
    --version                  Print version
    -A=<URL> --auth=<URL>      Authentication URL for swift
    -U=<USER> --user=<USER>    User name for swift auth
    -K=<KEY> --key=<KEY>       Key (password) for swift auth
    -p=<PREF> --prefix=<PREF>  String to prepend on container names [default: assembly]
    -S --skip-hash             Don't verify with md5 sums
    -n --dry-run               Don't upload, just report counts
    -v                         Show debug messages [default: False]
    -q                         Show only warnings [default: False]
"""
import getpass
import hashlib
import logging
import os
import re
import sys
from docopt import docopt
import pandas
from swiftclient.service import SwiftService, SwiftUploadObject

logger = logging.getLogger('to_swift')

##
# CONSTANTS
MISSING = 'missing'
UNKNOWN = 'unknown'
SAME = 'same'
DIFFERENT = 'different'
SUCCESS = 'success'
FAILED = 'failed'
SEGMENT_SIZE = 2.5 * 1024 * 1024 *1024
BLOCK_SIZE = 64 * 1024

# functions for checking files
EXCLUDES_FUNCTIONS = [
    os.path.islink,                 # skip links
    os.path.isdir,                  # skip folders
]

# list of regular expressions to exclude files from translation
EXCLUDES_EXPRESSIONS = [re.compile(pattern) for pattern in [
    r'_contigs\.fasta\.[a-z]+$',    # skip index of contigs
    r'\.bai$',                      # skip index of BAM file
    r'/core-[^/]+$',                # skip core files
    r'[_\.]ma?sh(\.log)?$',         # skip minhash files and logs
]]

# only supported option for now
AUTH_VERSION = '1.0'
SWIFT_KEY_VARS = {'auth': 'ST_AUTH',
                  'user': 'ST_USER',
                  'key': 'ST_KEY'}


# main function
def main(arguments):
    """
    connects to swift and uploads listed assemblies

    Attempts to print out some useful info
    """
    # set up logging
    if arguments['-v']:
        log_level = logging.DEBUG
    elif arguments['-q']:
        log_level = logging.WARN
    else:
        log_level = logging.INFO
    logging.basicConfig(stream=sys.stdout, level=log_level)

    suppress_swiftclient_warnings(log_level)

    logger.debug("docopt arguments:\n%r", arguments)

    # get swift params
    connection_options = {"auth_version": AUTH_VERSION, }
    missing_params = []
    for key, env_var in SWIFT_KEY_VARS.items():
        # try to get form arguments, fall back to env
        arg_key = '--' + key
        connection_options[key] = arguments.get(arg_key, None)
        if connection_options[key] is None:
            connection_options[key] = os.environ.get(env_var, None)
        if connection_options[key] is None:
            missing_params.append(key)

    if len(missing_params) > 0:
        if len(missing_params) == 1 and missing_params[0] == 'key':
            # ask user for password if that's all we're missing
            try:
                connection_options['key'] = getpass.getpass(
                    prompt='Password for {} at {}:'.format(
                        connection_options['user'],
                        connection_options['auth'],
                    )
                )
            except:
                print("Error getting authenticationkey from user")
                raise

        # Fail if we're missing needed connection options
        print("The following swift connection parameters are missing!\n{}"
              .format("\n".join(missing_params)))
        logger.debug(repr(arguments))
        sys.exit(1)

    # other options
    connection_options.update({
        'use_slo': False,
        'insecure': True,
        'segment_size': SEGMENT_SIZE,
    })

    # connect
    swift = SwiftService(options=connection_options)

    # upload assemblies
    total_counts = {}
    verify = not arguments['--skip-hash']
    dry_run = arguments['--dry-run']
    if dry_run:
        logger.warning("The following is just a dry run, nothing will be"
                       " uploaded")
    for assembly_path in arguments['<assembly>']:
        file_counts, failures = upload_assembly(assembly_path,
                                                swift,
                                                arguments['--prefix'],
                                                verify=verify,
                                                dry_run=dry_run,
                                               )
        logger.debug(file_counts)
        logger.debug("""Results from {assembly}
 {same} files correct on server
 {different} files wrong on server
 {missing} file missing on server
 {success} files uploaded successfully
 {failed} files failed to upload""".format(assembly=assembly_path,
                                           **file_counts))

        logger.debug("FAILURES from %s:\n%r", assembly_path, failures)
        if file_counts[FAILED] > 0:
            logger.info("%d files from %s failed to upload!",
                           file_counts[FAILED], assembly_path)
        
        # accumlate counts
        total_counts[os.path.basename(assembly_path)] = file_counts

    # turn counts into table
    counts_table = pandas.DataFrame(total_counts).T
    # sum counts across assembles
    file_counts = counts_table.sum().to_dict()
    # count assemblies with at least one in each category
    assembly_counts = (counts_table>0).sum().to_dict()
    logger.info("""Results from {count} assemblies:
 {same} files from {same_a} assemblies were correct on server
 {different} files from {different_a} assemblies were wrong on server
 {missing} files from {missing_a} assemblies were missing on server
 {success} files from {success_a} assemblies were uploaded successfully
 {failed} files from {failed_a} assemblies failed to upload"""
                .format(count=len(total_counts),
                        **file_counts,
                        **{i+"_a":assembly_counts[i] \
                           for i in assembly_counts},
                       ))
    print("Of %d assemblies, %d uploaded with %d failures" % \
          (len(total_counts),
           len(counts_table.query('missing + different > 0').index),
           len(counts_table.query('failed > 0').index)))
    if file_counts[FAILED] > 0:
        logger.warning("%d total files failed to upload!", file_counts[FAILED])
    if dry_run:
        logger.warning("The preceding was just a dry run, nothing was"
                       " uploaded")


def suppress_swiftclient_warnings(log_level):
    """ catch urrlib3 warnings about bad certs and suppress messages from swift
    """
    logging.captureWarnings(True)
    warner = logging.getLogger(name='py.warnings')
    warner.addFilter(lambda r: 0 if re.search('urllib3', r.pathname) else 1)
    if log_level >= logging.INFO:
        # hide all the connection messages
        sclogger = logging.getLogger(name='swiftclient.service')
        sclogger.addFilter(lambda r: 0)
        sclogger = logging.getLogger(name='swiftclient')
        sclogger.addFilter(lambda r: 0)


def file_not_excluded(file_name):
    """ Should we copy/rename file? """
    for excl_func in EXCLUDES_FUNCTIONS:
        if excl_func(file_name):
            return False
    for excl_expr in EXCLUDES_EXPRESSIONS:
        if excl_expr.search(file_name):
            return False
    return True


def get_assembly_files(assembly_dir):
    """ gets list of files to upload from assembly dir """
    return {f: os.path.join(assembly_dir, f)
            for f in os.listdir(assembly_dir)
            if file_not_excluded(os.path.join(assembly_dir, f))}


# from
# https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
def md5sum(filename,
           segment_size=SEGMENT_SIZE,
           blocksize=BLOCK_SIZE,
           hash_func=hashlib.md5):
    """
    Read file in chunks of blocksize and calculate a hash using hash_func.

    If the file is larger than segment_size, return list of hashes for each segment.

    Segment size must be a multiple of block size.
    """
    # blocksize needs to be divisor of segment size
    if segment_size % blocksize > 0:
        raise Exception("blocksize {} is not a divisor of segment size {}!"\
                        .format(blocksize, segment_size))

    hash_list = []
    hash_sum = hash_func()
    bytes_read = 0
    with open(filename, "rb") as file_handle:
        for block in iter(lambda: file_handle.read(blocksize), b""):
            hash_sum.update(block)
            bytes_read += blocksize
            if bytes_read >= segment_size:
                hash_list.append(hash_sum.hexdigest())
                hash_sum = hash_func()
                bytes_read = 0

    # process last (partial) chunk
    if bytes_read > 0:
        hash_list.append(hash_sum.hexdigest())

    # return
    if len(hash_list) == 0:
        # empty file, return hash of no data
        return hash_sum.hexdigest()
    elif len(hash_list) == 1:
        # small file -> single chunk
        return hash_list[0]
    else:
        # large file -> multiple chunks
        return hash_list


def check_files(swift, container, md5_dict):
    """
    Look for the indicated files in the given swift container

    md5_dict is map from object name to md5 sum

    return map from file names to status,
          which is one of (MISSING, SAME, DIFFERENT)
    """
    # get files in container
    for response in swift.stat(container, md5_dict.keys()):
        file_name = response['object']
        if not response['success']:
            status = MISSING
        else:
            loc_md5 = md5_dict.get(file_name, None)
            if loc_md5 is None:
                rem_md5 = None
            elif 'x-object-manifest' in response['headers']:
                # multi-part file, get segment md5s
                rem_md5 = get_segment_hashes(
                    swift,
                    response['headers']['x-object-manifest'],
                )
            else:
                rem_md5 = response['headers']['etag']
            if loc_md5 == rem_md5:
                status = SAME
            else:
                status = DIFFERENT
                logger.debug("Hashes don't match for %s:\n%s\n%s\n",
                             file_name, loc_md5, rem_md5)
        yield file_name, status


def get_segment_hashes(swift, object_manifest_path):
    """
    Get the hashes for this object's segments
    """
    segment_container, object_prefix = object_manifest_path.split('/', 1)
    response = list(swift.list(segment_container))[0]
    if not response['success']:
        return None

    hash_list = []
    for file_data in response['listing']:
        if file_data['name'].startswith(object_prefix):
            hash_list.append(file_data['hash'])
    return hash_list


def upload_assembly(assembly_dir, swift, container_prefix="assembly",
                    dry_run=False,
                    verify=True):
    """
    Looks for the files in an assembly folder
    Checks to see if they are in swift already
    Uploads missing or changed (by md5) files
    """

    # set up container name (assembly id is top folder (aka basename))
    container = "_".join((container_prefix, os.path.basename(assembly_dir)))

    # get list of files to upload
    upload_objects = []
    assembly_files = get_assembly_files(assembly_dir)
    logger.debug("Checking %d files before upload to %s",
                 len(assembly_files), container)

    if verify:
        # only upload ones not already on server
        assembly_md5s = {f: md5sum(p) for f, p in assembly_files.items()}
    else:
        assembly_md5s = {f: None for f in assembly_files.keys()}
    counts = {MISSING: 0, SAME: 0, DIFFERENT: 0, SUCCESS: 0, FAILED: 0}
    file_statuses = check_files(swift, container, assembly_md5s)

    for file_name, status in file_statuses:
        counts[status] += 1
        if status != SAME and not dry_run:
            upload_objects.append(SwiftUploadObject(assembly_files[file_name],
                                                    file_name))
    logger.debug("State of files in container %s:\n%r", container, counts)

    up_counts, failures, odd_responses = upload_assembly_files(swift,
                                                               container,
                                                               upload_objects,
                                                               assembly_md5s)
    counts.update(up_counts)
    if len(odd_responses) > 0:
        logger.warning("%d odd responses:\n%s",
                       len(odd_responses),
                       '\n'.join(repr(r) for r in odd_responses))

    return counts, failures


def upload_assembly_files(swift, container, upload_objects, md5_sums=None):
    """ upload and check response """
    failures = {}
    odd_responses = []
    counts = {SUCCESS: 0, FAILED: 0}
    if md5_sums is None:
        md5_sums = {}

    uploaded_segment_md5s = {}

    for response in swift.upload(container=container,
                                 objects=upload_objects):
        success = response['success']
        if response['action'] == 'create_container':
            logger.debug('Container %s %s',
                         response['container'],
                         'created successfully' if success else 'NOT created')

        elif response['action'] == 'upload_segment':
            for_object = response['for_object']
            seg_index = response['segment_index']
            seg_etag = response['segment_etag']
            # save segment hash (etag) in dict by object
            uploaded_segment_md5s\
                    .setdefault(for_object, {})[seg_index] = seg_etag
        elif response['action'] == 'upload_object':
            file_name = response['object']
            if success:
                if 'response_dict' in response:
                    uploaded_md5 = response['response_dict']['headers']['etag']
                else:
                    uploaded_md5 = [uploaded_segment_md5s[file_name][i] \
                                    for i in \
                                    range(len(uploaded_segment_md5s[file_name]))]

                if uploaded_md5 != md5_sums.get(file_name, uploaded_md5):
                    logger.info("%s uploaded, but hashes don't match: %s!=%s",
                                file_name, uploaded_md5, md5_sums[file_name])
                    success = False
            else:
                logger.info("%s not uploaded because %s",
                            file_name,
                            response['error'],
                           )
            counts[SUCCESS if success else FAILED] += 1
            logger.debug(file_name + (" " if success else " NOT ") + "uploaded")
            if not success:
                failures[file_name] = response
        else:
            odd_responses.append(response)

    if len(failures) > 0:
        logger.warning("%d files were not uploaded", len(failures))

    return counts, failures, odd_responses


# hook so that script behavior is contained in a main() function
if __name__ == '__main__':
    docopt_args = docopt(__doc__, version='Swift Uploader alpha 001')
    main(docopt_args)
